package com.itheima.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.constant.MessageConstant;
import com.itheima.dto.CategoryPageQueryDto;
import com.itheima.exception.BusinessException;
import com.itheima.mapper.CategoryMapper;
import com.itheima.pojo.Category;
import com.itheima.result.PageResult;
import com.itheima.service.CategoryService;
import com.itheima.utils.CurrentLoginHelper;
import com.itheima.vo.CategoryChildrenItemVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.util.List;

@Slf4j
@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryMapper categoryMapper;

    @Override
    public void save(Category category) {
        category.setCreateTime(LocalDateTime.now());
        category.setUpdateTime(LocalDateTime.now());
        category.setCreateUser(CurrentLoginHelper.getCurrentId());
        category.setUpdateUser(CurrentLoginHelper.getCurrentId());
        category.setLevel(category.getParentId() == 0?0:1);
        categoryMapper.insert(category);
    }

    @Override
    public void updateById(Category category) {
        category.setUpdateTime(LocalDateTime.now());
        category.setUpdateUser(CurrentLoginHelper.getCurrentId());

        categoryMapper.updateById(category);
    }

    @Override
    public void deleteById(Integer id) {
        //1. 判断该分类下是否还有子分类, 如果有, 不能删除, 提示信息
        List<Category> categoryList = categoryMapper.list(CategoryPageQueryDto.builder().parentId(id).build());
        if(!CollectionUtils.isEmpty(categoryList)){
            throw new BusinessException(MessageConstant.CATEGORY_NOT_DELETED);
        }

        //2. 删除分类
        categoryMapper.deleteById(id);
    }

    @Override
    public Category getById(Integer id) {
        return categoryMapper.getById(id);
    }

    @Override
    public PageResult page(CategoryPageQueryDto pageQueryDto) {
        //1. 设置请求参数
        PageHelper.startPage(pageQueryDto.getPage(), pageQueryDto.getPageSize());

        //2. 执行查询
        List<Category> categoryList = categoryMapper.list(pageQueryDto);
        Page<Category> page = (Page<Category>) categoryList;

        //3. 封装结果
        return new PageResult(page.getTotal(), page.getResult());
    }


    @Override
    public void updateNavStatus(Integer id, Integer status) {
        Category category = Category.builder().id(id)
                .navStatus(status)
                .updateTime(LocalDateTime.now())
                .updateUser(CurrentLoginHelper.getCurrentId())
                .build();
        categoryMapper.updateById(category);
    }


    @Override
    public List<Category> list(Integer parentId) {
        return categoryMapper.list(CategoryPageQueryDto.builder().parentId(parentId).build());
    }


    @Override
    public List<Category> findAll() {
        return categoryMapper.list(new CategoryPageQueryDto());
    }


    @Override
    public List<CategoryChildrenItemVo> findCategoryListWithChildren() {
        return categoryMapper.listWithChildren();
    }
}
