package com.itheima.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.constant.MessageConstant;
import com.itheima.dto.ProductPageQueryDto;
import com.itheima.exception.BusinessException;
import com.itheima.mapper.ProductMapper;
import com.itheima.pojo.Brand;
import com.itheima.pojo.Product;
import com.itheima.result.PageResult;
import com.itheima.service.ProductService;
import com.itheima.utils.CurrentLoginHelper;
import com.itheima.vo.ProductVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Slf4j
@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductMapper productMapper;

    @Override
    public PageResult page(ProductPageQueryDto pageQueryDto) {
        //1. 设置请求参数
        PageHelper.startPage(pageQueryDto.getPage(), pageQueryDto.getPageSize());

        //2. 执行查询
        List<ProductVo> categoryList = productMapper.list(pageQueryDto);
        Page<ProductVo> page = (Page<ProductVo>) categoryList;

        //3. 封装结果
        return new PageResult(page.getTotal(), page.getResult());
    }

    @Override
    public void save(Product product) {
        product.setCreateTime(LocalDateTime.now());
        product.setUpdateTime(LocalDateTime.now());
        product.setCreateUser(CurrentLoginHelper.getCurrentId());
        product.setUpdateUser(CurrentLoginHelper.getCurrentId());

        product.setPublishStatus(Product.PUBLISH_STATUS_DOWN);//默认下架
        product.setVerifyStatus(Product.VERIFY_STATUS_PENDING);//未审核

        productMapper.insert(product);
    }

    @Override
    public void updateById(Product product) {
        product.setUpdateTime(LocalDateTime.now());
        product.setUpdateUser(CurrentLoginHelper.getCurrentId());
        productMapper.update(product);
    }

    @Override
    public void delete(Integer id) {
        //1. 查询商品的状态
        Product product = productMapper.getById(id);
        if(product.getPublishStatus().equals(Product.PUBLISH_STATUS_UP)){
            throw new BusinessException(MessageConstant.PRODUCT_PUBLISH_CANNOT_DELETE);
        }

        //2. 删除商品
        productMapper.deleteById(id);
    }

    @Override
    public Product getById(Integer id) {
        return productMapper.getById(id);
    }

    @Override
    public void upOrDown(Integer id, Integer publishStatus) {
        Product product = Product.builder().id(id)
                .publishStatus(publishStatus)
                .updateTime(LocalDateTime.now())
                .updateUser(CurrentLoginHelper.getCurrentId())
                .build();
        productMapper.update(product);
    }


    @Override
    public void verify(Integer id, Integer status) {
        Product product = Product.builder().id(id)
                .verifyStatus(status)
                .updateTime(LocalDateTime.now())
                .updateUser(CurrentLoginHelper.getCurrentId())
                .build();
        productMapper.update(product);
    }
}
