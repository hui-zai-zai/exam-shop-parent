package com.itheima.controller;

import com.itheima.dto.CategoryPageQueryDto;
import com.itheima.pojo.Category;
import com.itheima.result.PageResult;
import com.itheima.result.Result;
import com.itheima.service.CategoryService;
import com.itheima.vo.CategoryChildrenItemVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * 分类管理
 */
@Slf4j
@Api(tags = "分类管理")
@RestController
@RequestMapping("/shop/categories")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    /**
     * 新增分类
     */
    @ApiOperation("新增分类")
    @PostMapping
    public Result save(@RequestBody Category category) {
        categoryService.save(category);
        return Result.success();
    }

    /**
     * 分页查询
     * @return
     */
    @ApiOperation("分页查询")
    @GetMapping("/list/{parentId}")
    public Result<PageResult> page(CategoryPageQueryDto pageQueryDto, @PathVariable Integer parentId){
        pageQueryDto.setParentId(parentId);
        PageResult pageResult = categoryService.page(pageQueryDto);
        return Result.success(pageResult);
    }

    /**
     * 更新分类
     */
    @ApiOperation("更新分类")
    @PutMapping
    public Result updateById(@RequestBody Category category) {
        categoryService.updateById(category);
        return Result.success();
    }

    /**
     * 删除分类
     */
    @ApiOperation("删除分类")
    @DeleteMapping("/{id}")
    public Result deleteById(@PathVariable Integer id) {
        categoryService.deleteById(id);
        return Result.success();
    }

    /**
     * 根据ID查询分类
     */
    @ApiOperation("根据ID查询分类")
    @GetMapping("/{id}")
    public Result getById(@PathVariable Integer id) {
        Category category = categoryService.getById(id);
        return Result.success(category);
    }

    /**
     * 修改显示状态
     */
    @ApiOperation("修改显示状态")
    @PutMapping("/navStatus/{id}/{sta}")
    public Result updateNavStatus(@PathVariable Integer id, @PathVariable Integer status){
        categoryService.updateNavStatus(id,status);
        return Result.success();
    }

    /**
     * 查询下级分类
     */
    @ApiOperation("查询下级分类")
    @GetMapping("/parent/{parentId}")
    public Result<List<Category>> list(@PathVariable Integer parentId){
        List<Category> categoryList = categoryService.list(parentId);
        return Result.success(categoryList);
    }


    /**
     * 查询全部
     */
    @ApiOperation("查询全部")
    @GetMapping("/all")
    public Result<List<Category>> findAll() {
        List<Category> categoryList = categoryService.findAll();
        return Result.success(categoryList);
    }


    @ApiOperation("查询分类及其子分类")
    @GetMapping("/list/withChildren")
    public Result<List<CategoryChildrenItemVo>> findCategoryListWithChildren(){
        List<CategoryChildrenItemVo> itemVoList = categoryService.findCategoryListWithChildren();
        return Result.success(itemVoList);
    }
}
