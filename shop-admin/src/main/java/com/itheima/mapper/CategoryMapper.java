package com.itheima.mapper;

import com.itheima.dto.CategoryPageQueryDto;
import com.itheima.pojo.Category;
import com.itheima.vo.CategoryChildrenItemVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface CategoryMapper {

    /**
     * 保存分类
     */
    void insert(Category category);

    /**
     * 修改分类
     */
    void updateById(Category category);

    /**
     * 删除分类
     */
    void deleteById(Integer id);

    /**
     * 根据ID查询分类
     */
    Category getById(Integer id);

    /**
     * 条件查询分类
     */
    List<Category> list(CategoryPageQueryDto pageQueryDto);

    /**
     * 查询分类及列表其子分类
     * @return
     */
    List<CategoryChildrenItemVo> listWithChildren();

}
