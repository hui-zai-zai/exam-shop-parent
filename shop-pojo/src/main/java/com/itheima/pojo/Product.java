package com.itheima.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 商品
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Product implements Serializable {

    private Integer id; //主键ID
    private Integer brandId; //品牌ID
    private Integer categoryId; //分类ID
    private String name; //商品名称
    private String pic; //商品图片
    private String sn; //货号
    private Integer publishStatus; //上架状态：0->下架；1->上架
    private Integer newStatus; //新品状态:0->不是新品；1->新品
    private Integer verifyStatus; //审核状态：0->未审核；1->审核通过 ; 2->审核未通过
    private BigDecimal price; //价格
    private Integer stock; //库存
    private String description; //商品描述
    private Integer lowStock; //库存预警值
    private Long weight; //商品重量，默认为克
    private LocalDateTime createTime; //创建时间
    private LocalDateTime updateTime; //修改时间
    private Integer createUser; //创建人
    private Integer updateUser; //修改人

    public static final Integer PUBLISH_STATUS_DOWN = 0; //下架
    public static final Integer PUBLISH_STATUS_UP = 1; //上架

    public static final Integer VERIFY_STATUS_PENDING = 0; //未审核
    public static final Integer VERIFY_STATUS_PASS = 1; //审核通过
    public static final Integer VERIFY_STATUS_FAIL = 2; //审核不通过

}
