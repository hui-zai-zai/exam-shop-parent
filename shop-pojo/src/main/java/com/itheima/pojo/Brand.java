package com.itheima.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 品牌
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Brand implements Serializable {

    private Integer id; //主键ID
    private String name; //品牌名称
    private String firstLetter; //首字母
    private Integer sort; //排序
    private Integer showStatus; //是否展示, 0: 展示 , 1: 展示
    private String logo; //品牌logo
    private String brandStory; //品牌故事
    private LocalDateTime createTime; //创建时间
    private LocalDateTime updateTime; //修改时间
    private Integer createUser; //创建人
    private Integer updateUser; //修改人

}
