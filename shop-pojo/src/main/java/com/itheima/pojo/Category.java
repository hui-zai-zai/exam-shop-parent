package com.itheima.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 分类
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Category implements Serializable {

    private Integer id; //主键ID
    private Integer parentId; //上机分类的编号：0 表示一级分类
    private String name; //分类名称
    private Integer level; //分类级别：0-> 1级；1-> 2级
    private Integer navStatus; //是否显示在导航栏：0->不显示；1->显示
    private Integer sort; //排序
    private String keywords; //关键字
    private String description; //描述
    private LocalDateTime createTime; //创建时间
    private LocalDateTime updateTime; //修改时间
    private Integer createUser; //创建人
    private Integer updateUser; //修改人

}
