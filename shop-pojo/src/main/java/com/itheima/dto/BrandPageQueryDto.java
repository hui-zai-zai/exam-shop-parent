package com.itheima.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BrandPageQueryDto {

    private int page; //页码
    private int pageSize; //每页展示记录数
    private String name; //品牌名称
    private int showStatus; //是否显式在导航栏 , 0: 不显示, 1: 显示
    private String firstLetter; //品牌首字母

}
