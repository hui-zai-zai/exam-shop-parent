package com.itheima.constant;

public class TokenConstant {

    public static final Integer LOGIN_TTL = 60; //过期时间
    public static final String TOKEN_HEADER = "token"; //请求头名称
    public static final String TOKEN_UNIQUE_FLAG = "uuid"; //请求头名称

}
